const fs = require('fs');

const matchesFnc = require('./matchesPlayedPerYear');
const matchPerYearFnc = require('./matchesWonPerYear');
const extraRunsFnc = require('./extraRunsConcededInYear2016');

const csvToJson = require("csvtojson");

csvToJson().fromFile('../data/matches.csv')
.then((matchJSON) =>{
    let matchPlayedPerYearResult = JSON.stringify(matchesFnc(matchJSON));
    fs.writeFile('../public/output/matchesPerYear.json', matchPlayedPerYearResult, (error)=>{
        if(error){
            console.log(error);
        } else{
            console.log("File created!!!");
        }
    });

    let matchWonPerYearResult = JSON.stringify(matchPerYearFnc(matchJSON));
    fs.writeFile('../public/output/matchesWonPerYear.json', matchWonPerYearResult, (error)=>{
        if(error){
            console.log(error);
        } else{
            console.log("File created!!!");
        }
    });

    csvToJson().fromFile('../data/deliveries.csv')
    .then((deliveriesJSON)=>{
        let extraRunsResult = JSON.stringify(extraRunsFnc(matchJSON, deliveriesJSON));
        fs.writeFile('../public/output/extraRunsConceded.json', extraRunsResult, (error)=>{
            if(error){
                console.log(error);
            }else{
                console.log("File created!!!");
            }
        });
    })
})