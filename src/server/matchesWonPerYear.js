 //Question : Number of matches won per team per year in IPL.

 function matchesWonPerYear(matchData){
    if(!matchData){
        return {};
    }
    const matchResultPerYear = matchData.reduce((year,match)=>{
        if(!year[match['season']]){
            year[match['season']] = {};
        }
        if(!year[match['season']][match['winner']]){
            year[match['season']][match['winner']] = 0; 
        }
        year[match['season']][match['winner']]++;
        return year;
    },{});

    return matchResultPerYear;
 }
module.exports=matchesWonPerYear;