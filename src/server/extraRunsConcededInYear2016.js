//Question : Extra runs conceded per team in the year 2016.

function extraRunsConcededInYear2016(matchData, deliveriesData, calculateYear=2016){
    if(!matchData || !deliveriesData){
        return {};
    }
    let validMatchID = matchData.reduce((matchId, matchDetail)=>{
        if(matchDetail['season'] == calculateYear){
            matchId[matchDetail['id']] = matchDetail['id'];
        }
        return matchId;
    },{});
    
    let extraRuns = deliveriesData.reduce((runs, delivery)=>{
        if(validMatchID[delivery['match_id']]){
            let battingTeam = delivery['batting_team'];
            if(!runs[battingTeam]){
                runs[battingTeam] = 0;
            }
            runs[battingTeam] += parseInt(delivery['extra_runs']);
        }
        return runs;
    },{});
    return extraRuns;
}
module.exports = extraRunsConcededInYear2016;